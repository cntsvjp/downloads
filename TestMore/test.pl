#!/usr/bin/perl
use strict;
use warnings;

# テスト数を指定する
use Test::More tests => 2; 

# ok 関数で試験が成功したかを確認。この試験は成功する。
my $num1 = 1;
ok($num1 == 1); 

# この試験は失敗する
my $num2 = 2;
ok($num2 == 1);
