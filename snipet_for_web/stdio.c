# $ gcc -o sample samle.c
# $ ./sample
# ABCD
# ef
# 1234
# G

#include        <stdio.h>
#include        <string.h>

int main(void)
{
    char str[] = "ABCD ef.1234.G";
    char *tp;

    /* スペース.を区切りに文字列を抽出 */   
    tp = strtok( str, " ." );
    puts( tp );
    while ( tp != NULL ) {
        tp = strtok( NULL," ." );
        if ( tp != NULL ) puts( tp );
    }

    return 0;
}
