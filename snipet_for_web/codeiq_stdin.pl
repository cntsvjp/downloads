
# perl
while(<>){
    chomp;
    print uc($_).$/;
}

# perl6
while my $s=get() {
    chomp($s);
    say uc $s;
}
