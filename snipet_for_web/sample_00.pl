#!/usr/bin/perl
use strict;
use warnings;

# $ perl -MO=Lint,all program.pl
# program.pl syntax OK

my $str = "Hello world!\n";
print $str;
